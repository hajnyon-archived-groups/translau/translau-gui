module.exports = {
    moduleNameMapper: {
        '^aurelia-binding$': '<rootDir>/node_modules/aurelia-binding'
    },
    modulePaths: [
        '<rootDir>/src',
        '<rootDir>/node_modules'
    ],
    moduleFileExtensions: [
        'ts',
        'js',
        'json'
    ],
    transform: {
        '^.+\\.(css|less|sass|scss|styl|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': 'jest-transform-stub',
        '^.+\\.ts$': 'ts-jest'
    },
    testRegex: '\\.spec\\.(ts|js)$',
    setupFiles: [
        '<rootDir>/test/jest-pretest.ts'
    ],
    testEnvironment: 'node',
    collectCoverage: false,
    collectCoverageFrom: [
        'src/**/*.{js,ts}',
        '!**/*.spec.{js,ts}',
        '!**/node_modules/**',
        '!**/test/**'
    ],
    coverageDirectory: '<rootDir>/test/coverage-jest',
    coverageReporters: [
        'json',
        'lcov',
        'text',
        'html'
    ]

};