import { IData } from '../services/data.interface';
import { IFile } from '../services/file.interface';
import { INamespaceStatus } from './namespace-status.interface';
import { EType } from './type.enum';

export class Namespace {

    public type: EType;
    public languages: Map<string, { value: string }> = new Map();
    public children: Map<string, Namespace> = new Map();
    public name: string = '';
    public status: INamespaceStatus = {
        missing: {
            translations: 0
        }
    };

    private buildNamespaces(language: string, data: IData): void {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                let namespace: Namespace | undefined = this.children.get(key);
                if (namespace) {
                    namespace.addFile(language, data[key], key);
                } else {
                    namespace = new Namespace();
                    namespace.addFile(language, data[key], key);
                }
                this.children.set(key, namespace);
            }
        }
    }

    private addMissing(languages: Set<string>): void {
        for (const language of languages.values()) {
            const hasKey: boolean = this.languages.has(language);
            if (!hasKey) {
                this.languages.set(language, { value: '' });
            }
        }
    }

    private getEmpty(): number {
        let empty: number = 0;
        for (const value of this.languages.values()) {
            if (value.value === '') {
                empty += 1;
            }
        }
        return empty;
    }

    public addFile(language: string, data: string | IData, name: string = ''): void {
        this.name = name;
        if (typeof data === 'string') {
            this.type = EType.VALUE;
            this.languages.set(language, { value: data });
        } else {
            this.type = EType.NAMESPACE;
            this.buildNamespaces(language, data);
        }
    }

    public postProcess(languages: Set<string>): number {
        switch (this.type) {
            case EType.VALUE:
                this.addMissing(languages);
                this.status.missing.translations = this.getEmpty();
            case EType.NAMESPACE:
                for (const namespace of this.children.values()) {
                    this.status.missing.translations += namespace.postProcess(languages);
                }
                break;
            default:
                break;
        }
        return this.status.missing.translations;
    }

    public extractFile(language: string): IFile {

        const data: IData = {};
        this.extract(language, data);

        return {
            language,
            data
        };
    }

    public extract(language: string, data: IData | string): void {

        if (this.type === EType.VALUE) {
            const value: { value: string } | undefined = this.languages.get(language);
            if (value !== undefined) {
                data[this.name] = value.value;
            }
            return;
        }

        for (const child of this.children.values()) {
            if (this.name !== '') {
                data[this.name] = data[this.name] || {};
                child.extract(language, data[this.name]);
            } else {
                child.extract(language, data);
            }
        }

    }

    public editLanguage(original: string, code: string | null): void {
        switch (this.type) {
            case EType.VALUE:
                const originalValue: { value: string } | undefined = this.languages.get(original);
                if (!originalValue) {
                    return;
                }
                if (code !== null) {
                    this.languages.set(code, JSON.parse(JSON.stringify(originalValue)));
                }
                this.languages.delete(original);
            case EType.NAMESPACE:
                for (const namespace of this.children.values()) {
                    namespace.editLanguage(original, code);
                }
                break;
            default:
                break;
        }
    }

    private validTranslationKey(key: string): void {
        if (key === '') {
            throw new Error('Translation key can\'t be empty.');
        }
        if (this.children.has(key)) {
            throw new Error(`Translation key '${key}' already exists.`);
        }
    }

    public addKey(key: string, languages: Set<string>): void {
        this.validTranslationKey(key);
        const namespace: Namespace = new Namespace();
        for (const language of languages) {
            namespace.addFile(language, '', key);
        }
        namespace.postProcess(languages);
        this.children.set(key, namespace);
    }

    public removeKey(key: string): void {
        if (!this.children.delete(key)) {
            throw new Error("Key doesn't exist.");
        }
    }

    public addNamespace(key: string, languages: Set<string>): void {
        this.validTranslationKey(key);
        const namespace: Namespace = new Namespace();
        namespace.addFile('', {}, key);
        namespace.postProcess(languages);
        this.children.set(key, namespace);
    }

    public removeNamespace(key: string): void {
        if (!this.children.delete(key)) {
            throw new Error("Namespace doesn't exist.");
        }
    }

}
