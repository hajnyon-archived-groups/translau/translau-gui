export interface IFolderStatus {
    missing: {
        translations: number;
        languages: number;
    };
}
