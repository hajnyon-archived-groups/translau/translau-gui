import { bindable, containerless } from 'aurelia-framework';
import { Namespace } from 'model/namespace.model';
import { Translations } from 'model/translations.model';

@containerless()
export class NamespacesCustomElement {

    @bindable public data: Translations;
    @bindable public namespace: Namespace = new Namespace();
    @bindable public selectedNamespace: Namespace = new Namespace();

    public select(namespace: Namespace): void {
        this.selectedNamespace = namespace;
    }

}
