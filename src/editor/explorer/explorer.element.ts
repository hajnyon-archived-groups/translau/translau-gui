import { bindable } from 'aurelia-framework';
import { Namespace } from 'model/namespace.model';
import { Translations } from 'model/translations.model';

export class ExplorerCustomElement {

    @bindable public data: Translations | null = null;
    @bindable public selectedNamespace: Namespace | null = null;

}
