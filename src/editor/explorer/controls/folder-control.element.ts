import { bindable, containerless, customElement } from 'aurelia-framework';
import { Folder } from 'model/folder.model';
import { showNotification } from 'model/notifications/notification.model';
import { Translations } from 'model/translations.model';
import * as uikit from 'uikit';

@containerless()
@customElement('folder-control')
export class FolderControl {
    @bindable public item: Folder;
    @bindable public data: Translations;

    protected async addFolder(): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Translation key to add:', '');
            if (key === null) {
                return;
            }
            this.item.addFolder(key, this.data.languages);
            showNotification(`<span uk-icon="icon: check"></span> <b>${key}</b> added.`, { status: 'success' });
        } catch (error) {
            console.error(error);
            showNotification(`<span uk-icon="icon: ban"></span> ${error.message}`, { status: 'danger', timeout: 0 });
        }
    }

    protected async removeFolder(item: Folder): Promise<void> {
        try {
            const key: string | null = await uikit.modal.prompt('Translation key to remove:', '');
            if (key === null) {
                return;
            }
            this.item.removeKey(key);
            showNotification(`<span uk-icon="icon: check"></span> <b>${key}</b> removed.`, { status: 'success' });
        } catch (error) {
            console.error(error);
            showNotification(`<span uk-icon="icon: ban"></span> ${error.message}`, { status: 'danger', timeout: 0 });
        }
    }
}
