import { bindable, containerless } from 'aurelia-framework';
import { showNotification } from 'model/notifications/notification.model';
import { Translations } from 'model/translations.model';
import uikit from 'uikit';

@containerless()
export class ControlsCustomElement {

    @bindable public data: Translations;

    public async addLanguage(): Promise<void> {
        try {
            const code: string | null = await uikit.modal.prompt('Language code:', '');
            if (code === null) {
                return;
            }
            this.data.addLanguage(code);
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    public async editLanguage(originalCode: string): Promise<void> {
        try {
            const code: string | null = await uikit.modal.prompt('Language code:', originalCode);
            if (code === null) {
                return;
            }
            this.data.editLanguage(originalCode, code);
        } catch (error) {
            console.error(error);
            showNotification(
                `<span uk-icon="icon: ban"></span> ${error.message}`,
                { status: 'danger', timeout: 0 }
            );
        }
    }

    public async removeLanguage(code: string): Promise<void> {
        try {
            await uikit.modal.confirm(`<h3><span uk-icon="trash" class="uk-margin-small-right">
            </span>Removing <b>${code}</b></h3>
            <p>Do you really want to remove language <b>${code}</b>?</p>
            <p class="uk-text-danger">All associated translations will be lost!</p>`);
            console.log('removing from ', code);
            this.data.editLanguage(code);
        } catch (error) {
            if (error) {
                console.error(error);
                showNotification(
                    `<span uk-icon="icon: ban"></span> ${error.message}`,
                    { status: 'danger', timeout: 0 }
                );
            }
        }
    }

}
