export class SortValueConverter {
    public toView(languages: Map<string, { value: string }>): Map<string, { value: string }> {
        return new Map([...languages.entries()].sort());
    }
}
