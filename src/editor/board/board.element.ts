import { bindable, customElement } from 'aurelia-framework';
import { Namespace } from 'model/namespace.model';

@customElement('board')
export class Board {

    @bindable public namespace: Namespace | null = null;

}
